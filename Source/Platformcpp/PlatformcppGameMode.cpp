// Copyright Epic Games, Inc. All Rights Reserved.

#include "PlatformcppGameMode.h"
#include "PlatformcppCharacter.h"
#include "UObject/ConstructorHelpers.h"

APlatformcppGameMode::APlatformcppGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
