// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Platformcpp : ModuleRules
{
	public Platformcpp(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "UMG" });
	}
}
