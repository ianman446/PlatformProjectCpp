// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PlatformcppGameMode.generated.h"

UCLASS(minimalapi)
class APlatformcppGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	APlatformcppGameMode();
};



